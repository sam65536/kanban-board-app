package uapp.studio.kanban.service;

import uapp.studio.kanban.domain.dto.ColumnDTO;

import java.util.List;
import java.util.Optional;

public interface ColumnService {

    List<ColumnDTO> getAllColumns();

    Optional<ColumnDTO> getColumnById(Long id);

    ColumnDTO createColumn(ColumnDTO columnDTO);

    ColumnDTO updateColumn(Long id, ColumnDTO columnDTO);

    void deleteColumn(Long id);

    void moveColumn(Long id, int position);
}
