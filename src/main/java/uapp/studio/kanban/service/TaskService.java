package uapp.studio.kanban.service;

import uapp.studio.kanban.domain.dto.TaskDTO;

import java.util.List;
import java.util.Optional;

public interface TaskService {

    List<TaskDTO> getAllTasks();

    Optional<TaskDTO> getTaskById(Long id);

    TaskDTO createTask(TaskDTO taskDTO);

    TaskDTO updateTask(Long id, TaskDTO taskDTO);

    void deleteTask(Long id);

    void moveToPosition(Long id, int position);

    void moveToColumn(Long id, Long columnId);
}
