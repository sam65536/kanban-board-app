package uapp.studio.kanban.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;
import uapp.studio.kanban.domain.dto.TaskDTO;
import uapp.studio.kanban.domain.entity.Column;
import uapp.studio.kanban.domain.entity.Task;
import uapp.studio.kanban.domain.mapper.TaskMapper;
import uapp.studio.kanban.repository.ColumnRepository;
import uapp.studio.kanban.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

    private final ColumnRepository columnRepository;
    private final TaskRepository taskRepository;
    private final TaskMapper mapper;

    @Override
    public List<TaskDTO> getAllTasks() {
        List<Task> tasks = new ArrayList<>();
        taskRepository.findAll().forEach(tasks::add);
        return tasks.stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @Override
    public Optional<TaskDTO> getTaskById(Long id) {
        return Optional.of(Optional.ofNullable(mapper.toDto(taskRepository.findById(id).get()))
                .orElseThrow(() -> new TaskNotFoundException(String.format("No task found with id: %d\n", id))));
    }

    @Override
    public TaskDTO createTask(TaskDTO taskDTO) {
        return mapper.toDto(taskRepository.save(mapper.toEntity(taskDTO)));
    }

    @Override
    public TaskDTO updateTask(Long id, TaskDTO taskDTO) {
        Optional<Task> optionalTask = taskRepository.findById(id);
        Task updatedTask = updateTaskFromDTO(optionalTask.get(), taskDTO);
        return mapper.toDto(taskRepository.save(updatedTask));
    }

    @Override
    public void deleteTask(Long id) {
        taskRepository.deleteById(id);
    }

    @Override
    public void moveToPosition(Long id, int position) {
        Task task = taskRepository.findById(id).get();
        Column column = task.getColumn();
        column.moveTask(id, position);
        columnRepository.save(column);
    }

    @Override
    public void moveToColumn(Long id, Long columnId) {
        Task task = taskRepository.findById(id).get();
        Column columnFrom = task.getColumn();
        Column columnTo = columnRepository.findById(columnId).get();
        columnFrom.removeTask(task);
        columnTo.addTask(task, columnTo.getTasks().size());
        columnRepository.save(columnFrom);
        columnRepository.save(columnTo);
    }

    private Task updateTaskFromDTO(Task task, TaskDTO taskDTO) {
        TaskDTO editedTask = mapper.toDto(task);

        if (Optional.ofNullable(taskDTO.getTitle()).isPresent()) {
            editedTask.setTitle(taskDTO.getTitle());
        }

        if (Optional.ofNullable((taskDTO.getDescription())).isPresent()) {
            editedTask.setDescription(taskDTO.getDescription());
        }

        if (Optional.ofNullable((taskDTO.getCreatedAt())).isPresent()) {
            editedTask.setCreatedAt(taskDTO.getCreatedAt());
        }

        if (Optional.ofNullable((taskDTO.getColumnId())).isPresent()) {
            editedTask.setColumnId(taskDTO.getColumnId());
        }

        if (taskDTO.getPosition() > 0) {
            editedTask.setPosition(taskDTO.getPosition());
        }
        return mapper.toEntity(editedTask);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    private class TaskNotFoundException extends RuntimeException {
        TaskNotFoundException(String message) {
            super(message);
        }
    }
}
