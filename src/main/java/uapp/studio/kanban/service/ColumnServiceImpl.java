package uapp.studio.kanban.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ResponseStatus;
import uapp.studio.kanban.domain.dto.ColumnDTO;
import uapp.studio.kanban.domain.entity.Column;
import uapp.studio.kanban.domain.mapper.ColumnMapper;
import uapp.studio.kanban.repository.ColumnRepository;
import uapp.studio.kanban.repository.TaskRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ColumnServiceImpl implements ColumnService {

    private final TaskRepository taskRepository;
    private final ColumnRepository columnRepository;
    private final ColumnMapper mapper;

    @Override
    public List<ColumnDTO> getAllColumns() {
        List<Column> columns = new ArrayList<>();
        columnRepository.findAll().forEach(columns::add);
        return columns.stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @Override
    public Optional<ColumnDTO> getColumnById(Long id) {
        Optional<Column> optionalColumn = columnRepository.findById(id);
        if (optionalColumn.isEmpty()) {
            throw new ColumnNotFoundException(String.format("No column found with id: %d", id));
        }
        return Optional.of(mapper.toDto(optionalColumn.get()));
    }

    @Override
    public ColumnDTO createColumn(ColumnDTO columnDTO) {
        columnDTO.setPosition(getAllColumns().size());
        return mapper.toDto(columnRepository.save(mapper.toEntity(columnDTO)));
    }

    @Override
    public ColumnDTO updateColumn(Long id, ColumnDTO columnDTO) {
        Optional<Column> optionalColumn = columnRepository.findById(id);
        if (optionalColumn.isEmpty()) {
            throw new ColumnNotFoundException(String.format("No column found with id: %d", id));
        }
        Column updatedColumn = updateColumnFromDTO(optionalColumn.get(), columnDTO);
        return mapper.toDto(columnRepository.save(updatedColumn));
    }

    @Override
    @Transactional
    public void deleteColumn(Long id) {
        Optional<Column> optionalColumn = columnRepository.findById(id);
        if (optionalColumn.isEmpty()) {
            throw new ColumnNotFoundException(String.format("No column found with id: %d", id));
        }
        taskRepository.deleteTasksByColumn(optionalColumn.get());
        columnRepository.delete(optionalColumn.get());
    }

    @Override
    public void moveColumn(Long id, int position) {
        Optional<Column> optionalColumn = columnRepository.findById(id);
        if (optionalColumn.isEmpty()) {
            throw new ColumnNotFoundException(String.format("No column found with id: %d", id));
        }
        List<Column> columns = new ArrayList<>((Collection<? extends Column>) columnRepository.findAll());
        columns.remove(optionalColumn.get());
        columns.add(position, optionalColumn.get());
        for (int i=0; i<columns.size(); i++) {
            columns.get(i).setPosition(i);
        }
        columnRepository.saveAll(columns);
    }

    private Column updateColumnFromDTO(Column column, ColumnDTO columnDTO) {
        ColumnDTO editedColumn = mapper.toDto(column);

        if (Optional.ofNullable(columnDTO.getTitle()).isPresent()) {
            editedColumn.setTitle(columnDTO.getTitle());
        }

        if (columnDTO.getPosition() > 0) {
            editedColumn.setPosition(columnDTO.getPosition());
        }

        return mapper.toEntity(editedColumn);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    private class ColumnNotFoundException extends RuntimeException {
        ColumnNotFoundException(String message) {
            super(message);
        }
    }
}
