package uapp.studio.kanban.repository;

import org.springframework.data.repository.CrudRepository;
import uapp.studio.kanban.domain.entity.Column;
import uapp.studio.kanban.domain.entity.Task;

public interface TaskRepository extends CrudRepository<Task, Long> {

    void deleteTasksByColumn(Column column);
}
