package uapp.studio.kanban.repository;

import org.springframework.data.repository.CrudRepository;
import uapp.studio.kanban.domain.entity.Column;

public interface ColumnRepository extends CrudRepository<Column, Long> {
}
