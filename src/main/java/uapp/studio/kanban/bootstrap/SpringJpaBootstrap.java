package uapp.studio.kanban.bootstrap;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import uapp.studio.kanban.domain.entity.Column;
import uapp.studio.kanban.domain.entity.Task;
import uapp.studio.kanban.repository.ColumnRepository;
import uapp.studio.kanban.repository.TaskRepository;

@Component
@Slf4j
@RequiredArgsConstructor
class SpringJpaBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private final int MIN_NUMBER_OF_COLUMNS = 3;
    private final int MAX_NUMBER_OF_COLUMNS = 6;
    private final int MIN_NUMBER_OF_TASKS_IN_COLUMN = 1;
    private final int MAX_NUMBER_OF_TASKS_IN_COLUMN = 10;

    private final TaskRepository taskRepository;
    private final ColumnRepository columnRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        loadKanban();
    }

    private void loadKanban() {
        int columnsNumber = MIN_NUMBER_OF_COLUMNS + (int) (Math.random() * MAX_NUMBER_OF_COLUMNS);
        for (int i = 0; i < columnsNumber; i++) {
            int tasksNumber =  MIN_NUMBER_OF_TASKS_IN_COLUMN + (int) (Math.random() * MAX_NUMBER_OF_TASKS_IN_COLUMN);
            Column column = new Column("column#" + i, i);
            columnRepository.save(column);
            for (int j = 0; j < tasksNumber; j++) {
                Task task = new Task("task#" + j, "just for test...", column);
                taskRepository.save(task);
            }
            columnRepository.save(column);
            log.info(String.format("column id:%d, contained %d tasks", column.getId(), column.getTasks().size()));
        }
    }
}