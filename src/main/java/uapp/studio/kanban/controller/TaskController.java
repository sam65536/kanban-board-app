package uapp.studio.kanban.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uapp.studio.kanban.domain.dto.TaskDTO;
import uapp.studio.kanban.service.TaskService;

import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/tasks")
public class TaskController {

    private final TaskService taskService;

    @GetMapping("/")
    public ResponseEntity<?> getAllTasks() {
        try {
            return new ResponseEntity<>(taskService.getAllTasks(), HttpStatus.OK);
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getTask(@PathVariable Long id) {
        try {
            Optional<TaskDTO> taskDb = taskService.getTaskById(id);
            return taskDb.<ResponseEntity<?>>map(taskDTO -> new ResponseEntity<>(taskDTO, HttpStatus.OK))
                    .orElseGet(() -> noTaskFoundResponse(id));
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @PostMapping("/")
    public ResponseEntity<?> createTask(@RequestBody TaskDTO taskDTO) {
        try {
            return new ResponseEntity<>(taskService.createTask(taskDTO), HttpStatus.CREATED);
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateTask(@PathVariable Long id, @RequestBody TaskDTO taskDTO) {
        try {
            Optional<TaskDTO> taskDb = taskService.getTaskById(id);
            if (taskDb.isEmpty()) {
                return noTaskFoundResponse(id);
            }
            return new ResponseEntity<>(taskService.updateTask(id, taskDTO), HttpStatus.OK);
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteTask(@PathVariable Long id) {
        try {
            Optional<TaskDTO> taskDb = taskService.getTaskById(id);
            if (taskDb.isEmpty()) {
                return noTaskFoundResponse(id);
            }
            taskService.deleteTask(id);
            return new ResponseEntity<>(String.format("Task with id: %d was deleted\n", id), HttpStatus.OK);
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @PutMapping("/{id}/moveto/position/{position}")
    public ResponseEntity<?> moveToPosition(@PathVariable Long id, @PathVariable int position) {
        try {
            Optional<TaskDTO> optionalTask = taskService.getTaskById(id);
            if (optionalTask.isEmpty()) {
                return noTaskFoundResponse(id);
            }
            taskService.moveToPosition(id, position);
            return new ResponseEntity<>(String.format
                    ("Task with id: %d was moved to position: %d\n", id, position), HttpStatus.OK);
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @PutMapping("/{id}/moveto/column/{columnId}")
    public ResponseEntity<?> moveToColumn(@PathVariable Long id, @PathVariable Long columnId) {
        try {
            Optional<TaskDTO> optionalTask = taskService.getTaskById(id);
            if (optionalTask.isEmpty()) {
                return noTaskFoundResponse(id);
            }
            taskService.moveToColumn(id, columnId);
            return new ResponseEntity<>(String.format
                    ("Task with id: %d was moved to column with id: %d\n", id, columnId), HttpStatus.OK);
        } catch (Exception e) {
            return errorResponse();
        }
    }

    private ResponseEntity<String> errorResponse() {
        return new ResponseEntity<>("Something went wrong :(\n", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<String> noTaskFoundResponse(Long id) {
        return new ResponseEntity<>(String.format("No task found with id: %d", id), HttpStatus.NOT_FOUND);
    }
}