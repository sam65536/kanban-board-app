package uapp.studio.kanban.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uapp.studio.kanban.domain.dto.ColumnDTO;
import uapp.studio.kanban.service.ColumnService;

import java.util.Optional;

@RestController
@RequestMapping("/columns")
@RequiredArgsConstructor
public class ColumnController {

    private final ColumnService columnService;

    @GetMapping("/")
    public ResponseEntity<?> getAllColumns() {
        try {
            return new ResponseEntity<>(columnService.getAllColumns(), HttpStatus.OK);
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getColumn(@PathVariable Long id) {
        try {
            Optional<ColumnDTO> columnDb = columnService.getColumnById(id);
            return columnDb.<ResponseEntity<?>>map(columnDTO -> new ResponseEntity<>(columnDTO, HttpStatus.OK))
                    .orElseGet(() -> noColumnFoundResponse(id));
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @PostMapping("/")
    public ResponseEntity<?> createColumn(@RequestBody ColumnDTO columnDTO) {
        try {
            return new ResponseEntity<>(columnService.createColumn(columnDTO), HttpStatus.CREATED);
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateColumn(@PathVariable Long id, @RequestBody ColumnDTO columnDTO) {
        try {
            Optional<ColumnDTO> columnDb = columnService.getColumnById(id);
            if (columnDb.isEmpty()) {
                return noColumnFoundResponse(id);
            }
            return new ResponseEntity<>(columnService.updateColumn(id, columnDTO), HttpStatus.OK);
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteColumn(@PathVariable Long id) {
        try {
            Optional<ColumnDTO> columnDb = columnService.getColumnById(id);
            if (columnDb.isEmpty()) {
                return noColumnFoundResponse(id);
            }
            columnService.deleteColumn(id);
            return new ResponseEntity<>(String.format("Column with id: %d was deleted\n", id), HttpStatus.OK);
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @GetMapping("/{id}/tasks")
    public ResponseEntity<?> getTasks(@PathVariable Long id) {
        try {
            Optional<ColumnDTO> columnDb = columnService.getColumnById(id);
            if (columnDb.isEmpty()) {
                return noColumnFoundResponse(id);
            }
            return new ResponseEntity<>(columnDb.get().getTasks(), HttpStatus.OK);
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @PutMapping("/{id}/moveto/{position}")
    public ResponseEntity<?> moveToPosition(@PathVariable Long id, @PathVariable int position) {
        try {
            Optional<ColumnDTO> columnDb = columnService.getColumnById(id);
            if (columnDb.isEmpty()) {
                return noColumnFoundResponse(id);
            }
            columnService.moveColumn(id, position);
            return new ResponseEntity<>(String.format
                    ("Column with id: %d was moved to position: %d\n", id, position), HttpStatus.OK);
        } catch (Exception e) {
            return errorResponse();
        }
    }

    private ResponseEntity<String> errorResponse() {
        return new ResponseEntity<>("Something went wrong :(\n", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<String> noColumnFoundResponse(Long id) {
        return new ResponseEntity<>(String.format("No column found with id: %d", id), HttpStatus.NOT_FOUND);
    }
}