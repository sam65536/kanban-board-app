package uapp.studio.kanban.domain.dto;

import lombok.Data;

@Data
public abstract class AbstractDTO {
    private Long id;
}
