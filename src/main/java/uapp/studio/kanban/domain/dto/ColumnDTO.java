package uapp.studio.kanban.domain.dto;

import lombok.*;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ColumnDTO extends AbstractDTO {

    private String title;
    private List<TaskDTO> tasks;
    private int position;
}
