package uapp.studio.kanban.domain.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uapp.studio.kanban.domain.dto.ColumnDTO;
import uapp.studio.kanban.domain.entity.Column;

@Component
public class ColumnMapper extends AbstractMapper<Column, ColumnDTO> {

    @Autowired
    public ColumnMapper() {
        super(Column.class, ColumnDTO.class);
    }
}
