package uapp.studio.kanban.domain.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uapp.studio.kanban.domain.dto.TaskDTO;
import uapp.studio.kanban.domain.entity.Task;
import uapp.studio.kanban.repository.ColumnRepository;

import javax.annotation.PostConstruct;
import java.util.Objects;

@Component
public class TaskMapper extends AbstractMapper<Task, TaskDTO> {

    private final ModelMapper mapper;
    private final ColumnRepository columnRepository;

    @Autowired
    public TaskMapper(ModelMapper mapper, ColumnRepository columnRepository) {
        super(Task.class, TaskDTO.class);
        this.mapper = mapper;
        this.columnRepository = columnRepository;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(Task.class, TaskDTO.class)
                .addMappings(m -> m.skip(TaskDTO::setColumnId))
                .setPostConverter(toDtoConverter());
        mapper.createTypeMap(TaskDTO.class, Task.class)
                .addMappings(m -> m.skip(Task::setColumn))
                .setPostConverter(toEntityConverter());
    }

    @Override
    public void mapSpecificFields(Task source, TaskDTO destination) {
        destination.setColumnId(getId(source));
    }

    private Long getId(Task source) {
        return Objects.isNull(source) || Objects.isNull(source.getId()) ? null : source.getColumn().getId();
    }

    @Override
    void mapSpecificFields(TaskDTO source, Task destination) {
        destination.setColumn(columnRepository.findById(source.getColumnId()).get());
    }
}