package uapp.studio.kanban.domain.mapper;

import uapp.studio.kanban.domain.dto.AbstractDTO;
import uapp.studio.kanban.domain.entity.AbstractEntity;

public interface Mapper<E extends AbstractEntity, D extends AbstractDTO> {

    E toEntity(D dto);

    D toDto(E entity);
}
