package uapp.studio.kanban.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static javax.persistence.CascadeType.ALL;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "board_column")
public class Column extends AbstractEntity {

    @OrderBy("position")
    @OneToMany(fetch = FetchType.EAGER, cascade = ALL, mappedBy = "column")
    private final List<Task> tasks = new ArrayList<>();

    public Column(String title, int position) {
        this.title = title;
        this.position = position;
    }

    public void moveTask(Long taskId, int position) {
        Optional<Integer> taskPosition = indexOfTask(taskId);
        if (taskPosition.isPresent()) {
            int index = taskPosition.get();
            Task task = this.tasks.remove(index);
            insertTask(task, position);
        } else {
            throw new IllegalArgumentException("Task with id <{" + taskId + "}> is not assigned to the column");
        }
    }

    public void addTask(Task task) {
        insertTask(task, this.tasks.size());
    }

    public void addTask(Task task, int position) {
        if (indexOfTask(task.getId()).isPresent()) {
            throw new IllegalArgumentException("Task with id <{" + task.getId() + "}> is already assigned to column");
        }
        insertTask(task, position);
    }

    public void removeTask(Task task) {
        Optional<Integer> taskPosition = indexOfTask(task.getId());
        taskPosition.ifPresent(pos -> this.tasks.remove(pos.intValue()));
        updateTasksPositions();
    }

    private Optional<Integer> indexOfTask(Long taskId) {
        Integer position = null;

        for (int i = 0; i < this.tasks.size(); i++) {
            if (taskId == this.tasks.get(i).getId()) {
                position = i;
                break;
            }
        }
        return Optional.ofNullable(position);
    }

    private void updateTasksPositions() {
        for (int i = 0; i < this.tasks.size(); i++) {
            this.tasks.get(i).setPosition(i);
        }
    }

    private void insertTask(Task task, int position) {
        this.tasks.add(position, task);
        task.setColumn(this);
        updateTasksPositions();
    }
}