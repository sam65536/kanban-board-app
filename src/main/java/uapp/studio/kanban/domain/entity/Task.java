package uapp.studio.kanban.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "task")
public class Task extends AbstractEntity {

    private String description;
    private LocalDateTime createdAt;

    @ManyToOne
    @JoinColumn(name = "column_id")
    private Column column;

    public Task(String title, String description, Column column) {
        this.createdAt = LocalDateTime.now();
        this.title = title;
        this.description = description;
        this.column = column;
        this.position = column.getTasks().size();
    }

    @PrePersist
    private void toCreate() {
        column.addTask(this);
    }

    @PreRemove
    private void toDelete() {
        column.removeTask(this);
    }
}
