package uapp.studio.kanban.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import uapp.studio.kanban.domain.dto.ColumnDTO;
import uapp.studio.kanban.domain.entity.Column;
import uapp.studio.kanban.domain.entity.Task;
import uapp.studio.kanban.domain.mapper.ColumnMapper;
import uapp.studio.kanban.repository.ColumnRepository;
import uapp.studio.kanban.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ColumnServiceTest {

    private ColumnService columnService;
    @Mock
    private TaskRepository taskRepository;
    @Mock
    private ColumnRepository columnRepository;
    @Mock
    private ColumnMapper mapper;

    @Before
    public void init() {
        columnService = new ColumnServiceImpl(taskRepository, columnRepository, mapper);
    }

    @Test
    public void when2ColumnsInDatabase_thenGetListWithAllOfThem() {
        mockColumnInDatabase(2);
        List<ColumnDTO> columns = columnService.getAllColumns();
        assertEquals(2, columns.size());
    }

    @Test
    public void whenTaskMoved_thenUpdateTasksPositions() {
        mockColumnInDatabase(3);
        List<Column> columns = createColumnList(3);
        createTaskList(columns.get(0), 5);
        columns.get(0).moveTask(1L, 4);
        assertEquals(Long.valueOf(1), columns.get(0).getTasks().get(4).getId());
        assertEquals(Long.valueOf(4), columns.get(0).getTasks().get(3).getId());
    }

    private void mockColumnInDatabase(int columnCount) {
        when(columnRepository.findAll())
                .thenReturn(createColumnList(columnCount));
    }

    private List<Column> createColumnList(int columnCount) {
        List<Column> columns = new ArrayList<>();
        IntStream.range(0, columnCount)
                .forEach(number -> {
                    Column column = new Column();
                    column.setId((long) number);
                    column.setTitle("Column" + number);
                    column.setPosition(number);
                    columns.add(number, column);
                });
        return columns;
    }

    private List<Task> createTaskList(Column column, int taskCount) {
        List<Task> tasks = new ArrayList<>();
        IntStream.range(0, taskCount)
                .forEach(number -> {
                    Task task = new Task();
                    task.setId((long) number);
                    task.setTitle("Task" + number);
                    task.setColumn(column);
                    column.setPosition(number);
                    tasks.add(number, task);
                });
        column.getTasks().addAll(tasks);
        return tasks;
    }
}
