package uapp.studio.kanban.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;
import uapp.studio.kanban.domain.dto.ColumnDTO;
import uapp.studio.kanban.domain.dto.TaskDTO;
import uapp.studio.kanban.domain.entity.Column;
import uapp.studio.kanban.domain.entity.Task;
import uapp.studio.kanban.repository.ColumnRepository;
import uapp.studio.kanban.repository.TaskRepository;

import java.util.Optional;

@TestPropertySource( properties = {
        "spring.datasource.url=jdbc:h2:mem:test",
        "spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.H2Dialect"
})
public class CommonITCase {

    @Autowired
    private ColumnRepository columnRepository;

    @Autowired
    private TaskRepository taskRepository;

    protected Column createSingleColumn() {
        Column column = new Column();
        int random = (int)(Math.random() * 100 + 1);
        column.setTitle("Test Column " + random);
        return column;
    }

    protected Task createSingleTask(Column column) {
        int random = (int)(Math.random() * 100 + 1);
        Task task = new Task("Test Task " + random, "Description " + random, column);
        return task;
    }

    protected ColumnDTO convertColumnToDTO(Column column) {
        return new ColumnDTO().builder()
                .title(column.getTitle())
                .build();
    }

    protected TaskDTO convertTaskToDTO(Task task) {
        return new TaskDTO().builder()
                .title(task.getTitle())
                .description(task.getDescription())
                .columnId(task.getColumn().getId())
                .position(task.getPosition())
                .build();
    }

    protected Column saveSingleRandomColumn() {
        return columnRepository.save(createSingleColumn());
    }


    protected Task saveSingleTask(Column column) {
        Task task = createSingleTask(column);
        columnRepository.save(column);
        return taskRepository.save(task);
    }

    protected Column saveSingleColumnWithOneTask() {
        Column column = saveSingleRandomColumn();
        saveSingleTask(column);
        return columnRepository.save(column);
    }

    protected Optional<Column> findColumnInDbById(Long id) {
        return columnRepository.findById(id);
    }

    protected Optional<Task> findTaskInDbById(Long id) {
        return taskRepository.findById(id);
    }
}
