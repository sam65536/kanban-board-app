package uapp.studio.kanban.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import uapp.studio.kanban.domain.dto.TaskDTO;
import uapp.studio.kanban.domain.entity.Column;
import uapp.studio.kanban.domain.entity.Task;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TaskControllerITCase extends CommonITCase {

    private String baseURL;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Before
    public void setUp(){
        baseURL = "http://localhost:" + port;
    }

    @Test
    public void whenGetAllTasks_thenReceiveSingleTask() {

        saveSingleTask(createSingleColumn());

        ResponseEntity<List<TaskDTO>> response = this.restTemplate.exchange(
                baseURL + "tasks/",
                HttpMethod.GET,
                new HttpEntity<>(new HttpHeaders()),
                new ParameterizedTypeReference<>() {
                });

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertTrue(response.getBody().size() >= 1);
    }

    @Test
    public void whenGetSingleTaskById_thenReceiveSingleTask() {

        Task task = saveSingleTask(createSingleColumn());

        ResponseEntity<TaskDTO> response = this.restTemplate.exchange(
                baseURL + "tasks/" + task.getId(),
                HttpMethod.GET,
                new HttpEntity<>(new HttpHeaders()),
                TaskDTO.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        TaskDTO responseTask = response.getBody();
        assertNotNull(responseTask.getId());
        assertEquals(task.getTitle(), responseTask.getTitle());
        assertEquals(task.getDescription(), responseTask.getDescription());
        assertEquals(task.getColumn().getId(), responseTask.getColumnId());
        assertEquals(task.getPosition(), responseTask.getPosition());
    }

    @Test
    public void whenPostSingleTask_thenItIsStoredInDb() {

        Column column = saveSingleRandomColumn();
        Task task = createSingleTask(column);

        ResponseEntity<TaskDTO> response = this.restTemplate.exchange(
                baseURL + "tasks/",
                HttpMethod.POST,
                new HttpEntity<>(convertTaskToDTO(task), new HttpHeaders()),
                TaskDTO.class);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());

        TaskDTO responseTask = response.getBody();
        assertNotNull(responseTask.getId());
        assertEquals(task.getTitle(), responseTask.getTitle());
        assertEquals(task.getDescription(), responseTask.getDescription());
        assertEquals(task.getColumn().getId(), responseTask.getColumnId());
        assertEquals(task.getPosition(), responseTask.getPosition());

        Task savedTask = findTaskInDbById(responseTask.getId()).get();
        assertEquals(responseTask.getId(), savedTask.getId());
        assertEquals(task.getTitle(), savedTask.getTitle());
        assertEquals(task.getDescription(), savedTask.getDescription());
        assertEquals(task.getColumn().getId(), savedTask.getColumn().getId());
        assertEquals(task.getPosition(), savedTask.getPosition());
    }

    @Test
    public void whenPostSingleTaskWithColumnAssignment_thenItIsStoredInDb() {

        Task task = createSingleTask(saveSingleRandomColumn());

        ResponseEntity<TaskDTO> response = this.restTemplate.exchange(
                baseURL + "tasks/",
                HttpMethod.POST,
                new HttpEntity<>(convertTaskToDTO(task), new HttpHeaders()),
                TaskDTO.class);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());

        TaskDTO responseTask = response.getBody();
        assertNotNull(responseTask.getId());
        assertEquals(task.getTitle(), responseTask.getTitle());
        assertEquals(task.getDescription(), responseTask.getDescription());

        Task savedTask = findTaskInDbById(responseTask.getId()).get();
        assertEquals(responseTask.getId(), savedTask.getId());
        assertEquals(task.getTitle(), savedTask.getTitle());
        assertEquals(task.getDescription(), savedTask.getDescription());
    }

    @Test
    public void whenPutSingleTask_thenItIsUpdated() {

        Task task = saveSingleTask(createSingleColumn());
        task.setTitle(task.getTitle() + " Updated");

        ResponseEntity<TaskDTO> response = this.restTemplate.exchange(
                baseURL + "tasks/" + task.getId(),
                HttpMethod.PUT,
                new HttpEntity<>(convertTaskToDTO(task), new HttpHeaders()),
                TaskDTO.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(task.getTitle(), findTaskInDbById(task.getId()).get().getTitle());
    }

    @Test
    public void whenDeleteSingleTaskById_thenItIsDeletedFromDb() {

        Task task = saveSingleTask(createSingleColumn());

        ResponseEntity<String> response = this.restTemplate.exchange(
                baseURL + "tasks/" + task.getId(),
                HttpMethod.DELETE,
                new HttpEntity<>(new HttpHeaders()),
                String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(String.format("Task with id: %d was deleted\n", task.getId()), response.getBody());
        assertFalse(findTaskInDbById(task.getId()).isPresent());
    }
}
