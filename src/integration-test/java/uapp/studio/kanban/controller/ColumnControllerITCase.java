package uapp.studio.kanban.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import uapp.studio.kanban.domain.dto.ColumnDTO;
import uapp.studio.kanban.domain.dto.TaskDTO;
import uapp.studio.kanban.domain.entity.Column;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ColumnControllerITCase extends CommonITCase {

    private String baseURL;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Before
    public void setUp(){
        baseURL = "http://localhost:" + port;
    }

    @Test
    public void whenGetAllColumns_thenReceiveSingleColumn() {

        saveSingleRandomColumn();

        ResponseEntity<List<ColumnDTO>> response = this.restTemplate.exchange(
                baseURL + "columns/",
                HttpMethod.GET,
                new HttpEntity<>(new HttpHeaders()),
                new ParameterizedTypeReference<>() {
                });

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertTrue(response.getBody().size() >= 1);
    }

    @Test
    public void whenGetSingleColumnById_thenReceiveSingleColumn() {

        Column column = saveSingleRandomColumn();

        ResponseEntity<ColumnDTO> response = this.restTemplate.exchange(
                baseURL + "columns/" + column.getId(),
                HttpMethod.GET,
                new HttpEntity<>(new HttpHeaders()),
                ColumnDTO.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(column.getId(), response.getBody().getId());
        assertEquals(column.getTitle(), response.getBody().getTitle());
    }

    @Test
    public void whenGetAllTasksForColumnById_thenReceiveTasksList() {

        Column column = saveSingleColumnWithOneTask();

        ResponseEntity<List<TaskDTO>> response = this.restTemplate.exchange(
                baseURL + "columns/" + column.getId() + "/tasks/",
                HttpMethod.GET,
                new HttpEntity<>(new HttpHeaders()),
                new ParameterizedTypeReference<>() {
                });

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(column.getTasks().get(0).getId(), response.getBody().get(0).getId());
        assertEquals(column.getTasks().get(0).getTitle(), response.getBody().get(0).getTitle());
        assertEquals(column.getTasks().get(0).getDescription(), response.getBody().get(0).getDescription());
        assertEquals(column.getTasks().get(0).getColumn().getId(), response.getBody().get(0).getColumnId());
    }

    @Test
    public void whenPostSingleColumn_thenItIsStoredInDb() {

        Column column = createSingleColumn();

        ResponseEntity<ColumnDTO> response = this.restTemplate.exchange(
                baseURL + "columns/",
                HttpMethod.POST,
                new HttpEntity<>(convertColumnToDTO(column), new HttpHeaders()),
                ColumnDTO.class);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());

        ColumnDTO responseColumn = response.getBody();
        assertNotNull(responseColumn.getId());
        assertEquals(column.getTitle(), responseColumn.getTitle());

        Column savedColumn = findColumnInDbById(responseColumn.getId()).get();
        assertEquals(column.getTitle(), savedColumn.getTitle());
    }

    @Test
    public void whenPutSingleColumn_thenItIsUpdated() {

        Column column = saveSingleRandomColumn();
        column.setTitle(column.getTitle() + " Updated");

        ResponseEntity<ColumnDTO> response = this.restTemplate.exchange(
                baseURL + "columns/" + column.getId(),
                HttpMethod.PUT,
                new HttpEntity<>(convertColumnToDTO(column), new HttpHeaders()),
                ColumnDTO.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(column.getTitle(), findColumnInDbById(column.getId()).get().getTitle());
    }

    @Test
    public void whenDeleteSingleColumnById_thenItIsDeletedFromDb() {

        Column column = saveSingleRandomColumn();

        ResponseEntity<String> response = this.restTemplate.exchange(
                baseURL + "columns/" + column.getId(),
                HttpMethod.DELETE,
                new HttpEntity<>(new HttpHeaders()),
                String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(String.format("Column with id: %d was deleted\n", column.getId()), response.getBody());
        assertFalse(findColumnInDbById(column.getId()).isPresent());
    }
}