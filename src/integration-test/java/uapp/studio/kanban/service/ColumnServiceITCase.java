package uapp.studio.kanban.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uapp.studio.kanban.domain.dto.ColumnDTO;
import uapp.studio.kanban.domain.entity.Column;
import uapp.studio.kanban.domain.mapper.ColumnMapper;
import uapp.studio.kanban.repository.ColumnRepository;
import uapp.studio.kanban.repository.TaskRepository;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ColumnServiceITCase {

    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private ColumnRepository columnRepository;
    @Autowired
    private ColumnMapper mapper;

    private ColumnService columnService;

    @Before
    public void init() {
        columnService = new ColumnServiceImpl(taskRepository, columnRepository, mapper);
    }

    @Test
    public void whenNewColumnCreated_thenColumnIsSavedInDb() {
        ColumnDTO columnDTO = ColumnDTO.builder()
                .title("Test Column")
                .build();

        columnService.createColumn(columnDTO);

        List<Column> columns = (List<Column>) columnRepository.findAll();

        assertTrue(columns.size() >= 1);
        assertNotNull(columns.get(columns.size() - 1));
        assertEquals("Test Column", columns.get(columns.size() - 1).getTitle());
    }
}
